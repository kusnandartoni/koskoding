import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TutorialRoutingModule } from './tutorial-routing.module';
import { TutorialComponent } from './tutorial.component';
import { MessagesComponent } from './component/messages/messages.component';
import { HeroesComponent } from './component/heroes/heroes.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { HeroDetailComponent } from './component/hero-detail/hero-detail.component';
import { FormsModule } from '@angular/forms';
import { HeroSearchComponent } from './component/hero-search/hero-search.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TutorialRoutingModule
  ],
  declarations: [
    TutorialComponent,
    DashboardComponent,
    MessagesComponent,
    HeroesComponent,
    HeroDetailComponent,
    HeroSearchComponent
  ]
})
export class TutorialModule { }
