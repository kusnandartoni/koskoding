import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-naturous',
  templateUrl: './naturous.component.html',
  styleUrls: ['./naturous.component.css']
})
export class NaturousComponent implements OnInit {
  naviToggle = false;
  load1 = false;
  load2 = false;
  load3 = false;
  load4 = false;
  constructor() { }

  ngOnInit() {
  }

  closeNaviToggle():void {
    this.naviToggle = false;
  }

  naviToggleBtnClick(toggle): void{
    this.naviToggle = toggle;
  }

}
