import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NaturousComponent } from './naturous.component';

describe('NaturousComponent', () => {
  let component: NaturousComponent;
  let fixture: ComponentFixture<NaturousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaturousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaturousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
