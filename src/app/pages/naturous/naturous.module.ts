import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NaturousRoutingModule } from './naturous-routing.module';
import { NaturousComponent } from './naturous.component';
import { FormsModule } from '@angular/forms';
import { DeferLoadModule } from "@trademe/ng-defer-load";

@NgModule({
  imports: [
    CommonModule,
    NaturousRoutingModule,
    FormsModule,
    DeferLoadModule
  ],
  declarations: [
    NaturousComponent
  ]
})
export class NaturousModule { }
