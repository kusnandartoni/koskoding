import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NaturousComponent } from './naturous.component';

const routes: Routes = [
  {
    path: '',
    component: NaturousComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NaturousRoutingModule { }
