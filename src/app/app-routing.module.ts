import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  { path: '', redirectTo: '/naturous', pathMatch: 'full'},
  { path: 'tutorial', loadChildren: './pages/tutorial/tutorial.module#TutorialModule'},
  { path: 'naturous', loadChildren: './pages/naturous/naturous.module#NaturousModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
